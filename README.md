:star::star::star: Storyes :star::star::star: <br>
to aplikacja pozwalająca kolekcjonować juz przeczytane książki, nawet jeśli nie mamy ich w fizycznej formie.

Strona logowania:
![img.png](img.png)

Strona główna:
![img_1.png](img_1.png)

Na głównej stronie widoczne są wszystkie książki dostepne w aplikacji.
Użytkownik z uprawnieniami administratora posiada opcję dodawania książek do bazy danych.

Na tej stronie można za pomocą ikonek: polubić daną książkę, dodać ją do własnej biblioteki.

Biblioteka użytkownika:
![img_2.png](img_2.png)

Na tej stronie użytkownik ma możliwość dodania notatki na temat danej książki, zapamiętując w ten sposób gdzie są np jego ulubione fragmenty.
Zawarta jest tutaj również jedna z kluczowych funkcjonalności, jaką jest polecanie książek. Jeżeli użytkownik nie ma książki która jest w biblitece jego przyjaciela z którym posiada wspólne tytuły, zostanie ona automatycznie polecona.

Użytkownicy:
![img_3.png](img_3.png)

Strona daje możliwość dodawania nowych przyjaciół. Im więcej ich masz tym więcej możliwych tytułów do odkrycia :smile:

Profil:
![img_4.png](img_4.png)

Na końcu znajdziesz stronę z twoimi osobistymi danymi. Nie jest na razie zbyt rozbudowana, ale to może się zmienić w niedługim czasie.
Znajdziesz tu także opcję wyloguj, ale nie korzystajcie z niej zbyt często :wink:
