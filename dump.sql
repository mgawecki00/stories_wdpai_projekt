create sequence "users_ID_user_seq"
	as integer;

alter sequence "users_ID_user_seq" owner to pprepcjmecujfk;

create sequence users_projects_comments_id_seq
	as integer;

alter sequence users_projects_comments_id_seq owner to pprepcjmecujfk;

create table users
(
	id integer default nextval('"users_ID_user_seq"'::regclass) not null
		constraint users_pk
			primary key,
	email varchar(255) not null,
	password varchar(255) not null,
	id_user_details integer default 0 not null,
	enabled boolean default false,
	salt integer,
	created_at date
);

alter table users owner to pprepcjmecujfk;

alter sequence "users_ID_user_seq" owned by users.id;

create table authors
(
	id serial
		constraint authors_pk
			primary key,
	name varchar(100) not null,
	surname varchar(100) not null
);

alter table authors owner to pprepcjmecujfk;

create table books
(
	id serial
		constraint books_pk
			primary key,
	title varchar(100) not null,
	description text not null,
	"like" integer default 0 not null,
	created_at date,
	id_author integer not null
		constraint books_authors_id_fk
			references authors
				on update cascade on delete cascade,
	image varchar(255)
);

alter table books owner to pprepcjmecujfk;

create unique index books_id_uindex
	on books (id);

create unique index authors_id_uindex
	on authors (id);

create table users_books
(
	id_user integer not null
		constraint user_users_books___fk
			references users
				on update cascade on delete cascade,
	id_book integer not null
		constraint book_users_books___fk
			references books
				on update cascade on delete cascade,
	id serial
		constraint users_books_pk
			primary key
);

alter table users_books owner to pprepcjmecujfk;

create unique index users_books_id_uindex
	on users_books (id);

create unique index users_books_id_user_id_book_uindex
	on users_books (id_user, id_book);

create table users_details
(
	id serial
		constraint users_details_pk
			primary key,
	name varchar(100) not null,
	surname varchar(100) not null,
	phone varchar,
	image varchar(100) default 'defUserPic.png'::character varying,
	role varchar(20) default 'user'::character varying not null
);

alter table users_details owner to pprepcjmecujfk;

create unique index users_details_id_uindex
	on users_details (id);

create table users_books_comments
(
	id integer default nextval('users_projects_comments_id_seq'::regclass) not null
		constraint users_projects_comments_pk
			primary key,
	comment varchar(255) not null,
	id_user_book integer
		constraint comment_users_projects_comments___fk
			references users_books
				on update cascade on delete cascade
);

alter table users_books_comments owner to pprepcjmecujfk;

alter sequence users_projects_comments_id_seq owned by users_books_comments.id;

create unique index users_projects_comments_id_uindex
	on users_books_comments (id);

create table friends
(
	id_user1 integer not null
		constraint friends_users_id_fk
			references users
				on update cascade on delete cascade,
	id_user2 integer not null
		constraint next_friends___fk
			references users
				on update cascade on delete cascade
);

alter table friends owner to pprepcjmecujfk;

create unique index friends_id_user1_id_user2_uindex
	on friends (id_user1, id_user2);

create function setcreatedat() returns trigger
	language plpgsql
as $$
BEGIN
    UPDATE users SET created_at = current_date where id IN (SELECT max(id) from users);

    RETURN NEW;
END;

$$;

alter function setcreatedat() owner to pprepcjmecujfk;

create trigger employee_insert_trigger
	after insert
	on users
	execute procedure setcreatedat();

