<?php

require 'Routing.php';

//phpinfo();

$path = trim($_SERVER['REQUEST_URI'], '/');
$path = parse_url($path, PHP_URL_PATH);

Routing::get('', 'DefaultController');
Routing::get('all_books', 'BooksController');

Routing::get('my_books', 'BooksController');
Routing::get('people', 'UsersController');
Routing::get('profile', 'UsersController');
Routing::get('notesForMobile', 'BooksController');

Routing::post('login', 'SecurityController');
Routing::post('register', 'SecurityController');
Routing::post('addBook', 'BooksController');
Routing::post('addPicture', 'UsersController');
Routing::post('search', 'BooksController');
Routing::post('searchUser', 'UsersController');
Routing::post('addNote', 'BooksController');

Routing::get('like', 'BooksController');
Routing::get('addToMyLib', 'BooksController');
Routing::get('addFriend', 'UsersController');

Routing::post('logout', 'SecurityController');

Routing::run($path);