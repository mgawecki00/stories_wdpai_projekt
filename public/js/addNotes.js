const addNote = document.querySelectorAll('input[placeholder="add note"]');
//const notesContainer = document.querySelectorAll('.notes');

addNote.forEach(button => button.addEventListener('keyup', function (event){
    if(event.key === "Enter"){
        event.preventDefault();

        const noteObj = this;
        const container = noteObj.parentElement.parentElement;
        const notesContainer = container.querySelector('.notes');
        const id = container.getAttribute("id");

        const data = {noteToAdd: this.value};

        fetch(`/addNote/${id}`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then(function (response){
            return response.json();
        }).then(function (note){
            appendNote(notesContainer,note)
        });
    }
}));

function appendNote(notesContainer, note){
    const template = document.querySelector("#note-template");

    const clone = template.content.cloneNode(true);
    const noteConst = clone.querySelector("p");
    noteConst.innerText = note;

    notesContainer.appendChild(clone);
}