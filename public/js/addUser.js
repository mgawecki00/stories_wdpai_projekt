const plusButtons = document.querySelectorAll(".fa-plus");

function addFriend() {
    const plus = this;
    const container = plus.parentElement.parentElement;
    const id = container.getAttribute("id");

    fetch(`/addFriend/${id}`)
        .then(window.location.reload())
}

plusButtons.forEach(button => button.addEventListener("click", addFriend));