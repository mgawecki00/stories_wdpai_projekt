const searchBooks = document.querySelector('input[placeholder="search books..."]');
const projectContainer = document.querySelector('.books');

searchBooks.addEventListener('keyup', function (event){
    if(event.key === "Enter"){
        event.preventDefault();

        const data = {search: this.value};

        fetch("/search", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then(function (response){
            return response.json();
        }).then(function (books){
           projectContainer.innerHTML = "";
           loadBooks(books)
        });
    }
});

function loadBooks(books){
    books.forEach(book => {
        console.log(book);
        createBook(book);
    })
}

function createBook(book){
    const template = document.querySelector("#book-template");

    const clone = template.content.cloneNode(true);
    const div = clone.querySelector("div");
    div.id = book.id;
    const image = clone.querySelector("img");
    image.src = `/public/img/uploads/${book.image}`;
    const title = clone.querySelector("h2");
    title.innerHTML = book.title;
    const author = clone.querySelector("h3");
    author.innerHTML = book.name + " " + book.surname;
    const description = clone.querySelector("p");
    description.innerHTML = book.description;
    const like = clone.querySelector(".fa-heart");
    like.innerText = book.like;
    const plus = clone.querySelector(".fa-plus");
    plus.addEventListener("click", addToMyLib);

    projectContainer.appendChild(clone);
}