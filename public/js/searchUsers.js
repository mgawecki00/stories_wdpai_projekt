const searchUsers = document.querySelector('input[placeholder="search people"]');
const peopleContainer = document.querySelector('.not-friends-yet');

searchUsers.addEventListener('keyup', function (event){
    if(event.key === "Enter"){
        event.preventDefault();

        const data = {search: this.value};

        fetch("/searchUser", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then(function (response){
            return response.json();
        }).then(function (people){
            peopleContainer.innerHTML = "";
            loadPeople(people)
        });
    }
});

function loadPeople(people){
    people.forEach(person => {
        console.log(person);
        createPerson(person);
    })
}
function createPerson(person){
    const template = document.querySelector("#person-template");

    const clone = template.content.cloneNode(true);
    const div = clone.querySelector("div");
    div.id = person.id;
    const image = clone.querySelector("img");
    image.src = `/public/img/uploads/${person.image}`;
    const nameSurname = clone.querySelector("p");
    nameSurname.innerHTML = person.name + " " + person.surname;

    peopleContainer.appendChild(clone);
}