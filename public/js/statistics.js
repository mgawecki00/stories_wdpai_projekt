const likeButtons = document.querySelectorAll(".fa-heart");
const plusButtons = document.querySelectorAll(".fa-plus");

function giveLike() {
    const likes = this;
    const container = likes.parentElement.parentElement.parentElement;
    const id = container.getAttribute("id");

    fetch(`/like/${id}`)
        .then(function (){
            likes.innerHTML = parseInt(likes.innerHTML) + 1;
        })
}

function addToMyLib() {
    console.log("Cos");
    const plus = this;
    const container = plus.parentElement.parentElement.parentElement;
    const id = container.getAttribute("id");

    fetch(`/addToMyLib/${id}`)
        .then()
}

likeButtons.forEach(button => button.addEventListener("click", giveLike));
plusButtons.forEach(button => button.addEventListener("click", addToMyLib));