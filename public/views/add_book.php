<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/all_books.css">
    <script src="https://kit.fontawesome.com/d6606babc0.js" crossorigin="anonymous"></script>
    <title>ADD BOOK</title>
</head>
<body>
<div class="base-container">
    <?php
    include('navigation.php');
    ?>
    <main>
        <aside class="in-all">
        </aside>
        <section class = "book-form">
            <h1>UPLOAD</h1>
            <form action="addBook" method="POST" ENCTYPE="multipart/form-data">
                <?php if(isset($messages)){
                    foreach ($messages as $message){
                        echo $message;
                    }
                }
                ?>
                <input name="title" type="text" placeholder="title">
                <input name="author" type="text" placeholder="author">
                <textarea name="description" rows="5" placeholder="description"></textarea>

                <input name="file" type="file">
                <button type="submit">send</button>
            </form>
        </section>
    </main>
</div>
</body>