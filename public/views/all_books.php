<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/all_books.css">
    <script src="https://kit.fontawesome.com/d6606babc0.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="./public/js/search.js" defer></script>
    <script type="text/javascript" src="./public/js/statistics.js" defer></script>
    <title>BOOKS</title>
</head>
<body>
        <?php
        include('navigation.php');
        ?>
        <main>
            <aside class="in-all">
                <div>
                    <?php if($_COOKIE['role'] === "admin") {?>
                    <a href="/addBook" class="button-add">
                        <i class="fas fa-plus"></i>
                        Add book
                    </a>
                    <?php } ?>
                </div>
                <div class = "search-bar">
                    <i class="fas fa-search"></i><!-- ikonka lupy -->
                    <input placeholder="search books..."> <!-- pole do wpisania -->
                </div>
            </aside>
            <section class = "books">
                <?php foreach ($books as $book): ?>
                <div id="<?= $book->getId(); ?>">
                    <img src="public/img/uploads/<?= $book->getImage(); ?>">
                    <div>
                        <h2><?= $book->getTitle(); ?></h2>
                        <h3><?= $book->getAuthor(); ?></h3>
                        <p><?= $book->getDesc(); ?></p>
                        <div class="social-section">
                            <i class="fas fa-heart"><?= $book->getLike(); ?></i>
                            <i class="fas fa-plus"></i>
                        </div>
                    </div>
                </div>
                <? endforeach; ?>
            </section>
        </main>
    </div>
</body>

<template id="book-template">
    <div id="">
        <img src="">
        <div>
            <h2>Title</h2>
            <h3>Author</h3>
            <p>Description</p>
            <div class="social-section">
                <i class="fas fa-heart">0</i>
                <i class="fas fa-plus"></i>
            </div>
        </div>
    </div>
</template>
