<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/all_books.css">
    <link rel="stylesheet" type="text/css" href="public/css/my_books.css">
    <script src="https://kit.fontawesome.com/d6606babc0.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="./public/js/addNotes.js" defer></script>
    <title>MY_LIBRARY</title>
</head>
<body>
<div class="base-container">
    <?php
    include('navigation.php');
    ?>
    <main>
        <aside class="recommended">
            <h1>Recommended for you:</h1>
            <div class="books">
                <?php foreach ($recommended as $book): ?>
                    <div id="<?= $book->getId(); ?>">
                        <img src="public/img/uploads/<?= $book->getImage(); ?>">
                        <div>
                            <h2><?= $book->getTitle(); ?></h2>
                            <h3><?= $book->getAuthor(); ?></h3>
                            <p><?= $book->getDesc(); ?></p>
                            <div class="social-section">
                                <i class="fas fa-heart"><?= $book->getLike(); ?></i>
                                <i class="fas fa-plus"></i>
                            </div>
                        </div>
                    </div>
                <? endforeach; ?>
            </div>
        </aside>
        <section class = "my_books">
            <?php foreach ($booksWithNotes as $bookWithNote): ?>
            <?php $book = $bookWithNote->getBook()?>
                <div id="<?= $book->getId(); ?>">
                    <img src="public/img/uploads/<?= $book->getImage(); ?>">
                    <div class="info">
                        <h2><?= $book->getTitle(); ?></h2>
                        <h3><?= $book->getAuthor(); ?></h3>
                        <p><?= $book->getDesc(); ?></p>
                        <input placeholder="add note"> <!-- pole do wpisania -->
                        <a href="/notesForMobile/<?= $book->getId(); ?>" class="button">
                            <i class="fas fa-plus"></i>
                            Show Info
                        </a>
                    </div>
                    <div class="notes">
                        <h3>
                            Notes:
                        </h3>
                        <div>
                            <?php foreach ($bookWithNote->getNotes() as $note): ?>
                                <p><?= $note; ?></p>
                            <? endforeach; ?>
                        </div>
                    </div>
                </div>
            <? endforeach; ?>
        </section>
    </main>
</div>
</body>

<template id="note-template">
    <p>Note</p>
</template>