<!DOCTYPE html>
<head>
</head>
<body>
<nav>
    <img src = "public/img/logo.svg">
    <ul>
        <li>
            <a href="/all_books" class="button">
                <i class="fas fa-book"></i>
                Books
            </a>
        </li>
        <li>
            <a href="/my_books" class="button">
                <i class="fas fa-book-reader"></i>
                My library
            </a>
        </li>
        <li>
            <a href="/people" class="button">
                <i class="fas fa-users"></i>
                People
            </a>
        </li>
        <li>
            <a href="/profile" class="button">
                <i class="fas fa-cog"></i>
                Settings
            </a>
        </li>
    </ul>
</nav>
</body>
