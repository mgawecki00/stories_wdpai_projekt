<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="/public/css/style.css">
    <link rel="stylesheet" type="text/css" href="/public/css/single_book.css">
    <script src="https://kit.fontawesome.com/d6606babc0.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="/./public/js/addNotes.js" defer></script>
    <title>ONE_BOOK</title>
</head>
<body>
<div class="base-container">
    <?php
    include('navigation.php');
    ?>
    <main>
        <section class="my_book">
            <?php $book = $bookWithNotes->getBook()?>
            <div id="<?= $book->getId(); ?>">
                <img src="/public/img/uploads/<?= $book->getImage(); ?>">
                <div class="info">
                    <h2><?= $book->getTitle(); ?></h2>
                    <h3><?= $book->getAuthor(); ?></h3>
                    <p><?= $book->getDesc(); ?></p>
                    <input placeholder="add note"> <!-- pole do wpisania -->
                    <a href="/notesForMobile/<?= $book->getId(); ?>" class="button">
                        <i class="fas fa-plus"></i>
                        Show Info
                    </a>
                </div>
                <div class="notes">
                    <h3>
                        Notes:
                    </h3>
                    <?php foreach ($bookWithNotes->getNotes() as $note): ?>
                        <p><?= $note; ?></p>
                    <? endforeach; ?>
                </div>
            </div>
        </section>
    </main>
</div>
</body>

<template id="note-template">
    <p>Note</p>
</template>