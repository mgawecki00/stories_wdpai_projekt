<!DOCTYPE html>
<head>
  <link rel="stylesheet" type="text/css" href="public/css/style.css">
  <link rel="stylesheet" type="text/css" href="public/css/people.css">
  <script src="https://kit.fontawesome.com/d6606babc0.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="./public/js/searchUsers.js" defer></script>
    <script type="text/javascript" src="./public/js/addUser.js" defer></script>
  <title>PEOPLE</title>
</head>
<body>
<div class="base-container">
    <?php
    include('navigation.php');
    ?>
  <main>
    <aside class="in-people">
      <div class = "search-bar">
          <i class="fas fa-search"></i><!-- ikonka lupy -->
          <input placeholder="search people"> <!-- pole do wpisania -->
      </div>
      <div class="not-friends-yet">
          <?php foreach ($users as $user): ?>
          <div id="<?= $user->getId(); ?>">
              <div class="social-section">
                  <i class="fas fa-plus"></i>
              </div>
              <img src="public/img/uploads/<?= $user->getImage(); ?>" width=1em height=1em>
              <p><?= $user->getName()." ".$user->getSurname(); ?></p>
          </div>
          <? endforeach; ?>
      </div>
    </aside>
    <section class = "people">
        <?php foreach ($friendsWithBooks as $friendWithBooks): ?>
        <?php $friend = $friendWithBooks->getUser(); ?>
      <div id <?= $friend->getId(); ?>>
        <div class="personal-info">
          <img src="public/img/uploads/<?= $friend->getImage(); ?>">
          <p><?= $friend->getName()." ".$friend->getSurname(); ?></p>
        </div>
        <div class="example-books">
          <p>Liked books:</p>
            <?php foreach ($friendWithBooks->getBooks() as $book): ?>
                <img src="public/img/uploads/<?= $book->getImage(); ?>">
            <?endforeach;?>
        </div>
      </div>
        <? endforeach; ?>
    </section>
  </main>
</div>
</body>

<template id="person-template">
    <div id="">
        <div class="social-section">
            <i class="fas fa-plus"></i>
        </div>
        <img src="" width=1em height=1em>
        <p>Name Surname</p>
    </div>
</template>