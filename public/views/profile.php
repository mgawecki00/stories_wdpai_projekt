<!DOCTYPE html>
<head>
  <link rel="stylesheet" type="text/css" href="public/css/style.css">
  <link rel="stylesheet" type="text/css" href="public/css/profile.css">
  <script src="https://kit.fontawesome.com/d6606babc0.js" crossorigin="anonymous"></script>
  <title>PROFILE</title>
</head>
<body>
<div class="base-container">
    <?php
    include('navigation.php');
    ?>
  <main>
    <section class="settings">
      <a href="/logout" class="button-out">
        <i class="fas fa-sign-out-alt"></i>
        Log Out
      </a>
      <div class="user">
        <div>
            <img src="public/img/uploads/<?= $user->getImage(); ?>">
            <a href="/addPicture" class="button-add">
                <i class="fas fa-pen"></i>
                Change Picture
            </a>
        </div>
        <div class="user-info">
          <div class="name">
            <p><?= $user->getName()." ".$user->getSurname(); ?></p>
          </div>
          <div class="user-personal-info">
            <p>Tel: <?= $user->getPhone(); ?></p>
            <p>Email: <?= $user->getEmail(); ?></p>
          </div>
        </div>
      </div>
    </section>
  </main>
</div>
</body>