<?php

class AppController {

    private $request;


    public function __construct()
    {
        $this->request = $_SERVER['REQUEST_METHOD'];
    }

    protected function isPost(): bool{
        return $this->request === 'POST';
    }

    protected function isGet(): bool{
        return $this->request === 'GET';
    }

    protected function render($template = null, array $variables = []){
        $templatePath = 'public/views/'.$template.'.php';
        $output = 'File not found';

        if(file_exists($templatePath)){
            extract($variables);

            ob_start();
            include $templatePath;
            $output = ob_get_clean();
        }

        print $output;
    }

    protected function checkCookie(){
        if(!isset($_COOKIE['id'])){
            die("Musisz się zalogować!");
        }
    }
}