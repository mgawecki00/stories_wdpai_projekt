<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/Book.php';
require_once __DIR__.'/../models/BookWithNotes.php';
require_once __DIR__.'/../repository/BookRepository.php';


class BooksController extends AppController{

    const MAX_FILE_SIZE = 1024 * 1024;
    const SUPPORTED_TYPES = ['image/png, image/jpeg'];
    const UPLOAD_DIRECTORY = '/../public/img/uploads/';

    private $messages = [];
    private $bookRepository;

    public function __construct()
    {
        parent::__construct();
        $this->bookRepository = new BookRepository();
    }

    public function all_books(){

        $this->checkCookie();

        $books = $this->bookRepository->getBooks();
        $this->render('all_books', ['books' => $books]);
    }

    public function my_books(){
        //TODO
        $this->checkCookie();

        $userId = $_COOKIE['id'];

        $booksWithNotes = $this->bookRepository->getMyBooks($userId);

        $recommended = $this->bookRepository->getRecommended($userId);

        $this->render('my_books', ['booksWithNotes' => $booksWithNotes , 'recommended' => $recommended]);
    }

    public function addBook(){
        $this->checkCookie();

        if($this->isPost() && is_uploaded_file($_FILES['file']['tmp_name']) && $this->validate($_FILES['file'])){

            move_uploaded_file(
                $_FILES['file']['tmp_name'],
                dirname(__DIR__).self::UPLOAD_DIRECTORY.$_FILES['file']['name']
            );

            $book = new Book($_POST['title'],$_POST['author'],$_POST['description'], $_FILES['file']['name'] );

            $this->bookRepository->addBook($book);

            return $this->render("all_books", [
                "messages" => $this->messages,
                "books" => $this->bookRepository->getBooks()
            ]);
        }

        $this->render("add_book", ["messages" => $this->messages]);

    }

    private function validate(array $file): bool{
        if($file['size'] > self::MAX_FILE_SIZE){
            $this->messages[] = 'File is too large for destination file system.';
            return false;
        }

        //var_dump($file);
        if(!isset($file['type']) && !in_array($file['type'], self::SUPPORTED_TYPES)){
            $this->messages[] = 'File is not supported.';
            return false;
        }

        return true;
    }

    public function search()
    {
        $this->checkCookie();
        $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';

        if($contentType === "application/json"){
            $content = trim(file_get_contents("php://input"));
            $decode = json_decode($content, true);

            header('Content-type: application/json');
            http_response_code(200);

            echo json_encode($this->bookRepository->getBookByTitle($decode['search']));
        }
    }

    public function like(int $id)
    {
        $this->checkCookie();
        $this->bookRepository->like($id);
        http_response_code(200);
    }

    //TODO dodawanie do własnej biblioteki

    public function addToMyLib(int $bookId)
    {
        $this->checkCookie();
        $userId = $_COOKIE['id'];
        $this->bookRepository->addToMyLib($userId,$bookId);
        http_response_code(200);
    }

    public function addNote(int $bookId){
        $this->checkCookie();
        $userId = $_COOKIE['id'];

        $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';

        if($contentType === "application/json"){
            $content = trim(file_get_contents("php://input"));
            $decode = json_decode($content, true);

            header('Content-type: application/json');
            http_response_code(200);

            echo json_encode($this->bookRepository->addNote($userId, $bookId,$decode['noteToAdd']));
        }
    }

    public function notesForMobile(int $bookId){
        $this->checkCookie();

        $userId = $_COOKIE['id'];

        $bookWithNotes = $this->bookRepository->getMyBook($userId, $bookId);
        //die(var_dump($bookWithNotes));

        $this->render("notes_for_mobile", ["bookWithNotes" => $bookWithNotes]);
    }
}
