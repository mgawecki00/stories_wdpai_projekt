<?php

require_once 'AppController.php';
require_once __DIR__.'/../repository/UserRepository.php';
require_once __DIR__.'/../models/User.php';

class SecurityController extends AppController{

    private $userRepository;

    public function __construct()
    {
        parent::__construct();
        $this->userRepository = new UserRepository();
    }

    public function logout(){
        //TODO usuniecie ciasteczka i przekierowanie na logowanie
        $this->userRepository->updateUserPresence($_COOKIE['id'], false);
        setcookie('id', "", time()-3600);
        setcookie('email', "", time()-3600);
        setcookie('role', "", time()-3600);
        $this->render('login',['messages' => ['Wylogowano']]);
    }

    public function login(){

        if(!$this->isPost()){
            return $this->render('login');
        }

        $email = $_POST['email'];
        $password = md5($_POST['password']);

        $user = $this->userRepository->getUser($email);

        if(!$user){
            return $this->render('login',['messages' => ['User not exist!']]);
        }

        if($user->getEmail() !== $email){
            return $this->render('login',['messages' => ['User with this email not exist']]);
        }

        if($user->getPassword() !== $password){
            return $this->render('login',['messages' => ['Wrong password']]);
        }

        //TODO ciasteczko
        if(!isset($_COOKIE['id'])){
            setcookie("id", $user->getId(), time() + 3600);
            setcookie("email", $user->getEmail(), time() + 3600);
            setcookie("role", $user->getRole(), time() + 3600);
        }

        $this->userRepository->updateUserPresence($_COOKIE['id'], true);

//        return $this->render('all_books');
        $url = "http://$_SERVER[HTTP_HOST]";
        header("Location: {$url}/all_books");
    }

    public function register()
    {
        if (!$this->isPost()) {
            return $this->render('register');
        }

        $email = $_POST['email'];
        $password = $_POST['password'];
        $confirmedPassword = $_POST['confirmedPassword'];
        $name = $_POST['name'];
        $surname = $_POST['surname'];
        $phone = $_POST['phone'];

        if(!$email || !$password || !$name || !$surname || !$phone){
            return $this->render('register', ['messages' => ['Fill all boxes']]);
        }

        if ($password !== $confirmedPassword) {
            return $this->render('register', ['messages' => ['Please provide proper password']]);
        }

        //TODO try to use better hash function
        $user = new User($email, md5($password), $name, $surname);
        $user->setPhone($phone);

        $this->userRepository->addUser($user);

        return $this->render('login', ['messages' => ['You\'ve been succesfully registrated!']]);
    }
}