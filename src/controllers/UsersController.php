<?php

require_once 'AppController.php';
require_once __DIR__.'/../repository/UserRepository.php';
require_once __DIR__.'/../models/User.php';
require_once __DIR__.'/../models/UsersWithBooks.php';

class UsersController extends AppController{
    const MAX_FILE_SIZE = 1024 * 1024;
    const SUPPORTED_TYPES = ['image/png, image/jpeg'];
    const UPLOAD_DIRECTORY = '/../public/img/uploads/';

    private $userRepository;
    private $messages = [];

    public function __construct()
    {
        parent::__construct();
        $this->userRepository = new UserRepository();
    }

    public function people(){
        $this->checkCookie();

        $userId = $_COOKIE['id'];

        $friendsWithBooks = $this->userRepository->getUserFriends($userId);
        $users = $this->userRepository->getUsers($userId);

        $this->render('people', ['friendsWithBooks' => $friendsWithBooks, 'users' => $users]);
    }

    public function profile(){
        $this->checkCookie();

        $userEmail = $_COOKIE['email'];

        $user = $this->userRepository->getUser($userEmail);

        $this->render('profile', ['user' => $user]);
    }

    public function searchUser()
    {
        $this->checkCookie();
        $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';

        if($contentType === "application/json"){
            $content = trim(file_get_contents("php://input"));
            $decode = json_decode($content, true);

            header('Content-type: application/json');
            http_response_code(200);

            echo json_encode($this->userRepository->getUserByName($decode['search']));
        }
    }

    public function addFriend(int $friendId){
        $userId = $_COOKIE['id'];

        $this->userRepository->addFriend($userId, $friendId);
    }

    public function addPicture()
    {
        $this->checkCookie();

        if($this->isPost() && is_uploaded_file($_FILES['file']['tmp_name']) && $this->validate($_FILES['file'])){

            move_uploaded_file(
                $_FILES['file']['tmp_name'],
                dirname(__DIR__).self::UPLOAD_DIRECTORY.$_FILES['file']['name']
            );

            $this->userRepository->updatePic($_COOKIE['id'], $_FILES['file']['name']);

            return $this->render("profile", [
                "messages" => $this->messages,
                "user" => $this->userRepository->getUser($_COOKIE['email'])
            ]);
        }

        $this->render("add_picture", ["messages" => $this->messages]);

    }

    private function validate(array $file): bool{
        if($file['size'] > self::MAX_FILE_SIZE){
            $this->messages[] = 'File is too large for destination file system.';
            return false;
        }

        //var_dump($file);
        if(!isset($file['type']) && !in_array($file['type'], self::SUPPORTED_TYPES)){
            $this->messages[] = 'File is not supported.';
            return false;
        }

        return true;
    }
}
