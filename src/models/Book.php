<?php

class  Book {
    private $title;
    private $author;
    private $desc;
    private $image;
    private $like;
    private $id;

    public function __construct($title, $author, $desc, $image, $like = 0, $id = null)
    {
        $this->title = $title;
        $this->author = $author;
        $this->desc = $desc;
        $this->image = $image;
        $this->like = $like;
        $this->id = $id;
    }

    public function getLike()
    {
        return $this->like;
    }


    public function setLike($like): void
    {
        $this->like = $like;
    }


    public function getId()
    {
        return $this->id;
    }


    public function setId($id): void
    {
        $this->id = $id;
    }



    public function getTitle(): string
    {
        return $this->title;
    }


    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    public function getAuthor() : string
    {
        return $this->author;
    }


    public function setAuthor(string $author)
    {
        $this->author = $author;
    }

    public function getDesc() : string
    {
        return $this->desc;
    }


    public function setDesc(string $desc)
    {
        $this->desc = $desc;
    }


    public function getImage(): string
    {
        return $this->image;
    }


    public function setImage(string $image)
    {
        $this->image = $image;
    }


}
