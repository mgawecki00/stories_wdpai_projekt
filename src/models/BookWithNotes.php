<?php

class BookWithNotes {
    private $book;
    private $Notes;

    public function __construct($book, $Notes)
    {
        $this->book = $book;
        $this->Notes = $Notes;
    }

    public function getBook()
    {
        return $this->book;
    }

    public function getNotes()
    {
        return $this->Notes;
    }
}
