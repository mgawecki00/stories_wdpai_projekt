<?php

class User
{
    const ROLE_USER = 'user';
    const ROLE_ADMIN = 'admin';


    private $email;
    private $password;
    private $name;
    private $surname;
    private $phone;
    private $Id;
    private $Image;
    private $role;

    public function __construct(string $email,string $password,string $name,string $surname)
    {
        $this->email = $email;
        $this->password = $password;
        $this->name = $name;
        $this->surname = $surname;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function setRole(string $role): void
    {
        if($role === "admin"){
            $this->role = self::ROLE_ADMIN;
        }else{
            $this->role = self::ROLE_USER;
        }
    }

    public function getImage()
    {
        return $this->Image;
    }

    public function setImage($Image): void
    {
        $this->Image = $Image;
    }

    public function getId()
    {
        return $this->Id;
    }

    public function setId($Id): void
    {
        $this->Id = $Id;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setPhone($phone): void
    {
        $this->phone = $phone;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password)
    {
        $this->password = $password;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getSurname(): string
    {
        return $this->surname;
    }

    public function setSurname(string $surname)
    {
        $this->surname = $surname;
    }


}