<?php

class UsersWithBooks {
    private $user;
    private $books;

    public function __construct($user, $books)
    {
        $this->user = $user;
        $this->books = $books;
    }

    public function getBooks()
    {
        return $this->books;
    }

    public function getUser()
    {
        return $this->user;
    }
}
