<?php

require_once 'Repository.php';
require_once __DIR__."/../models/Book.php";
require_once __DIR__."/../models/BookWithNotes.php";

class bookRepository extends Repository
//SELECT * FROM public.books WHERE id = :id JOIN public.authors ON public.books.id_author = public.authors.id
{
    public function getBook(int $id): ?Book
    {
        $stmt = $this->database->connect()->prepare('
        SELECT * FROM public.books WHERE id = :id
        ');

        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $book = $stmt->fetch(PDO::FETCH_ASSOC);

        //TODO metoda zwracajaca exception
        if($book == false){
            return null;
        }

        return new Book(
            $book['title'],
            'Adam Mickiewicz',
            //$book['author'],
            $book['description'],
            $book['image']);
    }

    public function addBook(Book $book): void
    {
        $nameAndSurname = ucwords(strtolower($book->getAuthor()));

        $array = explode(" ",$nameAndSurname);

        $stmtA = $this->database->connect()->prepare('
        SELECT id FROM authors where name = :givenName AND surname = :givenSurname
        ');

        $stmtA->bindParam(':givenName' , $array[0], PDO::PARAM_STR);
        $stmtA->bindParam(':givenSurname' , $array[1], PDO::PARAM_STR);
        $stmtA->execute();

        $authorId = $stmtA->fetch(PDO::FETCH_ASSOC);

        if($authorId == false)
        {
            $stmtaddA = $this->database->connect()->prepare('
            INSERT INTO authors (name,surname)
            VALUES(?,?)
            ');

            $stmtaddA->execute([
                $array[0],
                $array[1]
            ]);


            $stmtgetId = $this->database->connect()->prepare('
            SELECT id FROM authors where name = :givenName AND surname = :givenSurname
            ');

            $stmtgetId->bindParam(':givenName' , $array[0], PDO::PARAM_STR);
            $stmtgetId->bindParam(':givenSurname' , $array[1], PDO::PARAM_STR);

            $stmtgetId->execute();

            $authorId = $stmtgetId->fetch(PDO::FETCH_ASSOC);

            $ID = $authorId['id'];
        }
        else
        {
            $ID = $authorId['id'];
        }

        $date = new DateTime();
        $stmt = $this->database->connect()->prepare('
        INSERT INTO books (title,description,created_at,id_author,image)
        VALUES (?, ?, ?, ?, ?)
        ');

        //$id_author = 5;//Adam Mickiewicz
        $stmt->execute([
            $book->getTitle(),
            $book->getDesc(),
            $date->format('Y-m-d'),
            $ID,
            $book->getImage()
        ]);
    }

    public function getBooks(): array
    {
        $result =[];

        $stmt = $this->database->connect()->prepare('
        SELECT books.id,title,name,surname,description,image,"like" FROM books JOIN authors on books.id_author = authors.id
        ');

        $stmt->execute();

        $books = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($books as $book){
            $result[] = new Book(
                $book['title'],
                $book['name'].' '.$book['surname'],
                $book['description'],
                $book['image'],
                $book['like'],
                $book['id']
            );
        }

        return $result;
    }

    public function getBookByTitle(string $searchString)
    {
        $searchString = '%'.strtolower($searchString).'%';

        $stmt = $this->database->connect()->prepare('
        SELECT books.id, title, description, "like", image, "name", surname FROM books JOIN authors on books.id_author = authors.id where LOWER(title) LIKE :search
        ');

        $stmt->bindParam(':search', $searchString, PDO::PARAM_STR);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function like(int $id){
        $stmt = $this->database->connect()->prepare('
        UPDATE books SET "like" = "like" + 1 WHERE id = :id
        ');

        $stmt->bindParam(":id", $id, PDO::PARAM_INT);
        $stmt->execute();
    }

    public function getMyBooks($userId) : array
    {
        $result = [];

        $stmt = $this->database->connect()->prepare('
        SELECT ub.id as node, title,name,surname,description,image,"like",b.id as id FROM users_books ub JOIN books b on ub.id_book =b.id JOIN authors on b.id_author = authors.id where ub.id_user = :id 
        ');

        $stmt->bindParam(":id", $userId, PDO::PARAM_INT);
        $stmt->execute();

        $books = $stmt->fetchAll(PDO::FETCH_ASSOC);



        foreach ($books as $book){
            $result[] = new BookWithNotes(
                new Book(
                $book['title'],
                $book['name'].' '.$book['surname'],
                $book['description'],
                $book['image'],
                $book['like'],
                $book['id']),
                $this->getNotes($book['node']));
        }

        return $result;
    }

    public function addToMyLib(int $userId, int $bookId)
    {
        $stmt = $this->database->connect()->prepare('
        INSERT INTO users_books (id_user, id_book)
        VALUES (?, ?)
        ');

        $stmt->execute([
            $userId,
            $bookId
        ]);
    }

    public function addNote(int $userId, int $bookId, string $noteToAdd)
    {
        $stmt = $this->database->connect()->prepare('
        SELECT id from users_books where id_user = :userId and id_book = :bookId
        ');

        $stmt->bindParam(":userId", $userId, PDO::PARAM_INT);
        $stmt->bindParam(":bookId", $bookId, PDO::PARAM_INT);
        $stmt->execute();

        $id_user_book = $stmt->fetch(PDO::FETCH_ASSOC);

        $stmt = $this->database->connect()->prepare('
        INSERT INTO users_books_comments (comment, id_user_book)
        VALUES (?,?)
        ');

        $stmt->execute([
            $noteToAdd,
            $id_user_book['id']
        ]);

        return $noteToAdd;
    }

    public function getRecommended($userId) : array
    {
        $result = [];

        $stmt = $this->database->connect()->prepare('
        SELECT ub.id_user, count(ub.id_book) from users_books ub where ub.id_book IN (SELECT ub2.id_book from users_books ub2 where ub2.id_user = :idUser ) and ub.id_user <> :idUser group by ub.id_user order by count DESC
        ');

        $stmt->bindParam(":idUser", $userId, PDO::PARAM_INT);
        $stmt->execute();

        $id_user_counts = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach($id_user_counts as $id_user_count){
         //die(var_dump($id_user_count['id_user']));

            $stmt = $this->database->connect()->prepare('
            SELECT *, b.id as bId from books b JOIN authors a on a.id = b.id_author JOIN users_books ub on b.id = ub.id_book where ub.id_user = :userId and ub.id_book NOT IN (SELECT ub2.id_book from users_books ub2 where ub2.id_user = :targetUserId) LIMIT 2
            ');

            $stmt->bindParam(":userId", $id_user_count['id_user'], PDO::PARAM_INT);
            $stmt->bindParam(":targetUserId", $userId, PDO::PARAM_INT);
            $stmt->execute();

            $notMyBooks = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if($notMyBooks)
            {
                foreach ($notMyBooks as $notMyBook){
                    $result[] = new Book(
                        $notMyBook['title'],
                        $notMyBook['name'].' '.$notMyBook['surname'],
                        $notMyBook['description'],
                        $notMyBook['image'],
                        $notMyBook['like'],
                        $notMyBook['id']);
                }

                break;
            }
        }

        return $result;
    }

    public function getMyBook($userId, int $bookId)
    {
        $stmt = $this->database->connect()->prepare('
        SELECT ub.id as node, title,name,surname,description,image,"like",b.id as id FROM users_books ub JOIN books b on ub.id_book =b.id JOIN authors on b.id_author = authors.id where ub.id_user = :idUser and b.id = :idBook
        ');

        $stmt->bindParam(":idUser", $userId, PDO::PARAM_INT);
        $stmt->bindParam(":idBook", $bookId, PDO::PARAM_INT);
        $stmt->execute();

        $book = $stmt->fetch(PDO::FETCH_ASSOC);

        return $result = new BookWithNotes(
            new Book(
                $book['title'],
                $book['name'].' '.$book['surname'],
                $book['description'],
                $book['image'],
                $book['like'],
                $book['id']),
            $this->getNotes($book['node']));
    }

    private function getNotes($nodeId): array
    {
        $result = [];

        $stmt = $this->database->connect()->prepare('
        SELECT comment FROM users_books_comments c where c.id_user_book = :id 
        ');

        $stmt->bindParam(":id", $nodeId, PDO::PARAM_INT);
        $stmt->execute();


        $notes = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($notes as $note){
            $result[] = $note['comment'];
        }

        return $result;
    }
}