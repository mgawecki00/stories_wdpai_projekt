<?php

require_once 'Repository.php';
require_once __DIR__."/../models/User.php";
require_once __DIR__."/../models/UsersWithBooks.php";

class UserRepository extends Repository {

    public function getUser(string $email): ?User
    {
        $stmt = $this->database->connect()->prepare('
        SELECT * FROM users u LEFT JOIN users_details ud 
            ON u.id_user_details = ud.id WHERE email = :email
        ');

        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        //TODO metoda zwracajaca exception
        if($user == false){
            return null;
        }

        $userRet =  new User(
            $user['email'],
            $user['password'],
            $user['name'],
            $user['surname']);
        $userRet->setId($user['id']);
        $userRet->setPhone($user['phone']);
        $userRet->setImage($user['image']);
        $userRet->setRole($user['role']);

        return $userRet;
    }

    ///////////////////////////////////////////////////////////////////////////////////

    public function addUser(User $user)
    {
        $stmt = $this->database->connect()->prepare('
            INSERT INTO users_details (name, surname, phone)
            VALUES (?, ?, ?)
        ');

        $stmt->execute([
            $user->getName(),
            $user->getSurname(),
            $user->getPhone()
        ]);

        $stmt = $this->database->connect()->prepare('
            INSERT INTO users (email, password, id_user_details)
            VALUES (?, ?, ?)
        ');

        $stmt->execute([
            $user->getEmail(),
            $user->getPassword(),
            $this->getUserDetailsId($user)
        ]);
    }

    public function getUserDetailsId(User $user): int
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.users_details WHERE name = :name AND surname = :surname AND phone = :phone
        ');
        $name = $user->getName();
        $surname = $user->getSurname();
        $phone = $user->getPhone();

        $stmt->bindParam(':name', $name, PDO::PARAM_STR);
        $stmt->bindParam(':surname',$surname, PDO::PARAM_STR);
        $stmt->bindParam(':phone', $phone, PDO::PARAM_STR);
        $stmt->execute();

        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        return $data['id'];
    }

    public function getUserFriends(int $userId) : array
    {
        $result = [];

        $stmt = $this->database->connect()->prepare('
            SELECT *, u.id as userid FROM friends f JOIN users u ON f.id_user2 = u.id JOIN users_details ud ON ud.id = u.id_user_details WHERE f.id_user1 = :id
        ');

        $stmt->bindParam(':id', $userId, PDO::PARAM_INT);
        $stmt->execute();

        $users = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($users as $user){
            $userRet = new User(
                $user['email'],
                $user['password'],
                $user['name'],
                $user['surname']
            );
            $userRet->setId($user['userid']);
            $userRet->setPhone($user['phone']);
            $userRet->setImage($user['image']);

            $result[] = new UsersWithBooks(
                $userRet,
                $this->getFriendsBooks($userRet->getId())
                );
        }

        return $result;
    }

    public function getUsers(int $userId) : array{
        $result =[];

        $stmt = $this->database->connect()->prepare('
        SELECT *, u.id as userid FROM users u JOIN users_details ud on u.id_user_details = ud.id WHERE u.id <> :id
        ');

        $stmt->bindParam(":id", $userId, PDO::PARAM_INT);
        $stmt->execute();

        $users = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($users as $user){
            $userRet = new User(
                $user['email'],
                $user['password'],
                $user['name'],
                $user['surname']
            );
            $userRet->setId($user['userid']);
            $userRet->setPhone($user['phone']);
            $userRet->setImage($user['image']);

            $result[] = $userRet;
        }

        return $result;
    }

    public function getUserByName(string $searchString)
    {
        $searchString = '%' . strtolower($searchString) . '%';

        $stmt = $this->database->connect()->prepare("
        SELECT * FROM users u JOIN users_details ud on u.id_user_details = ud.id  where LOWER(CONCAT(name,' ',surname)) LIKE :search
        ");

        $stmt->bindParam(':search', $searchString, PDO::PARAM_STR);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function addFriend(int $userId, int $friendId){

        $stmt = $this->database->connect()->prepare('
            INSERT INTO friends (id_user1, id_user2)
            VALUES (?, ?)
        ');

        $stmt->execute([
            $userId,
            $friendId
        ]);
    }

    public function updatePic($userId, $fileName)
    {
        $stmt = $this->database->connect()->prepare('
        SELECT id_user_details FROM users where id = :id
        ');

        $stmt->bindParam(':id', $userId, PDO::PARAM_INT);
        $stmt->execute();

        $id_details = $stmt->fetch(PDO::FETCH_ASSOC);

        $stmt = $this->database->connect()->prepare('
            UPDATE users_details SET image = :file WHERE id = :id
        ');

        $stmt->bindParam(":id", $id_details['id_user_details'], PDO::PARAM_INT);
        $stmt->bindParam(":file", $fileName, PDO::PARAM_STR);
        $stmt->execute();
    }

    public function updateUserPresence($givenId, $boolBool)
    {
        $stmt = $this->database->connect()->prepare('
            UPDATE users SET enabled = :arg WHERE id = :parid
        ');

        $stmt->bindParam(":parid", $givenId, PDO::PARAM_INT);
        $stmt->bindParam(":arg", $boolBool, PDO::PARAM_BOOL);
        $stmt->execute();
    }

    private function getFriendsBooks(int $friendId) : array
    {
        $result = [];

        $stmt = $this->database->connect()->prepare('
        SELECT ub.id as node, title,name,surname,description,image,"like",b.id as id FROM users_books ub JOIN books b on ub.id_book =b.id JOIN authors on b.id_author = authors.id where ub.id_user = :id LIMIT 4
        ');

        $stmt->bindParam(":id", $friendId, PDO::PARAM_INT);
        $stmt->execute();

        $books = $stmt->fetchAll(PDO::FETCH_ASSOC);



        foreach ($books as $book){
            $result[] = new Book(
                $book['title'],
                $book['name'].' '.$book['surname'],
                $book['description'],
                $book['image'],
                $book['like'],
                $book['id']);

        }

        return $result;
    }
}
